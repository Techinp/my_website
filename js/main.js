
// Start Map

function initMap() {
    var center = {lat: 13.744773, lng: 100.494765};

    var locations = [
        ['Rangsit University<br>\
        52/347, Paholyothin Rd., Lak Hok, Muang Pathum Thani, Pathum Thani 12000 Thailand<br>\
       <a href="https://goo.gl/maps/M4xWGJq7tH22" target="_blank">Get Directions</a>',   13.9610847, 100.5865471],
        ['Rajabopit Shcool<br>\
        3, Sanam Chai Rd., Phra Borom Maha Ratchawang, Phra nakhon, Bangkok 10200 Thailand<br>\
       <a href="https://goo.gl/maps/3w5qUq1Qjc82" target="_blank">Get Directions</a>', 13.744773, 100.494765],
        ['Samutsakhonburana School<br>\
        919, Norrasing road., Mueang Samut Sakhon, Samut Sakhon 74000 Thailand<br>\
       <a href="https://goo.gl/maps/DjHoKqmhiqN2" target="_blank">Get Directions</a>', 13.548364, 100.275497]
      ];

      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: center
      });
      
    var infowindow =  new google.maps.InfoWindow({}); // Create Map
    var marker, count;
    for (count = 0; count < locations.length; count++) {
        marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[count][1], locations[count][2]), // get Lat lng
        map: map,
        title: locations[count][0] // get name place
        });
        google.maps.event.addListener(marker, 'click', (function (marker, count) { 
          return function () {
              infowindow.setContent(locations[count][0]);
              infowindow.open(map, marker); // open map 
          }; // return 
        })(marker, count));
    }
}

// End Map


// Jump to
function gotoIntroduce(){
    document.getElementById('Introduce').scrollIntoView();
}
function gotoAboutme(){
    document.getElementById('Aboutme').scrollIntoView();
}
function gotoSkill() {
    document.getElementById('Skills').scrollIntoView();
}
function gotoEducation(){
    document.getElementById('Education').scrollIntoView();
}
function gotoInterest(){
    document.getElementById('Interest').scrollIntoView();
}

// End Jump to
